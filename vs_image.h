/*
 * vs_image.h
 *
 *  Created on: Dec 18, 2012
 *      Author: szhu
 */

#ifndef VS_IMAGE_H_
#define VS_IMAGE_H_

#include <stdint.h>
#include <glib.h>
#include <gst/video/video.h>

typedef struct _VSImage VSImage;

struct _VSImage {
    GstVideoFormat format;

    int nb_plane;
    struct{
        guint8 *data;
        guint8 *rel_data;
        int width;
        int height;
        int size;
        int rel_width;
        int rel_height;
        int rel_top;
        int rel_left;
    }plane[4];

    int width, height;

    int aspect_ratio_u8;
};

int vs_image_init(GstVideoFormat format, uint8_t *buf, int w, int h, VSImage* img);
const guint8 *vs_image_get_black (GstVideoFormat format);
int vs_image_scale(guint8* pool_buf, VSImage* des, VSImage* src, gboolean adjust_aspect);
int vs_image_set_real(VSImage* des, VSImage* src);
int vs_image_set_buffer(VSImage* img, guint8* buf, gboolean des_buffer);

#endif /* VS_IMAGE_H_ */
