/* 
 * GStreamer
 * Copyright (C) 2006 Stefan Kost <ensonic@users.sf.net>
 * Copyright (C) 2012 szhu <<user@hostname.org>>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __GST_MVIDEOSCALE_H__
#define __GST_MVIDEOSCALE_H__

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include <gst/video/video.h>
#include <stdint.h>

#include "vs_image.h"

G_BEGIN_DECLS

#define GST_TYPE_MVIDEOSCALE \
  (gst_mvideo_scale_get_type())
#define GST_MVIDEOSCALE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_MVIDEOSCALE,GstMVideoScale))
#define GST_MVIDEOSCALE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_MVIDEOSCALE,GstMVideoScaleClass))
#define GST_IS_MVIDEOSCALE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_MVIDEOSCALE))
#define GST_IS_MVIDEOSCALE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_MVIDEOSCALE))

typedef struct _GstMVideoScale      GstMVideoScale;
typedef struct _GstMVideoScaleClass GstMVideoScaleClass;

struct _GstMVideoScale {
  GstBaseTransform element;

  GstVideoFormat format;
  gboolean keep_source_aspect;
  VSImage des_img;
  VSImage src_img;

  uint8_t *tbuf;
  int   tbuf_size;

  gboolean interlaced;

  gboolean silent;
};

struct _GstMVideoScaleClass {
  GstBaseTransformClass parent_class;
};

GType gst_mvideo_scale_get_type (void);

G_END_DECLS

#endif /* __GST_MVIDEOSCALE_H__ */
