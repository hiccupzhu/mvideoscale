CFLAGS=\
	-DGST_PACKAGE='"GStreamer"' -DGST_ORIGIN='"http://gstreamer.net"' \
	-DVERSION='"0.0"' -DHAVE_USER_MTU -Wall -Wimplicit -g \
	`pkg-config --cflags gstreamer-controller-0.10` \
	`pkg-config --cflags liboil-0.3` \
#	`pkg-config --cflags orc-0.4`
	
LDFLAGS=`pkg-config --libs gstreamer-controller-0.10`\
		`pkg-config --libs liboil-0.3`\
#		`pkg-config --libs orc-0.4`

libgstmvideoscale.la: libgstmvideoscale.lo vs_image.lo scale.lo
	libtool --mode=link gcc -module -avoid-version -rpath /usr/local/lib/gstreamer-0.10/ -export-symbols-regex gst_plugin_desc\
	 -o libgstmvideoscale.la  $+ -lgstvideo-0.10 $(LDFLAGS)  -lgstbase-0.10

libgstmvideoscale.lo: gstmvideoscale.c
	libtool --mode=compile gcc $(CFLAGS) -o $@ -c $<
	
vs_image.lo:vs_image.c vs_image.h
	libtool --mode=compile gcc $(CFLAGS) -O2 -o $@ -c $<

scale.lo:scale.c
	libtool --mode=compile gcc $(CFLAGS) -O2 -o $@ -c $<

gstmvideoscaleorc.lo: gstmvideoscaleorc.c gstmvideoscaleorc.h
	libtool --mode=compile gcc $(CFLAGS) -o $@ -c $<
	
gstmvideoscaleorc.c:gstmvideoscaleorc.orc
	orcc --implementation --include glib.h -o gstmvideoscaleorc.c $<

gstmvideoscaleorc.h:gstmvideoscaleorc.orc
	orcc --header --include glib.h -o gstmvideoscaleorc.h $<
	
.PHONY: install

install: libgstmvideoscale.la
	libtool --mode=install install libgstmvideoscale.la /usr/local/lib/gstreamer-0.10/

clean:
	rm -rf *.o *.lo *.a *.la .libs
	rm -f gstmvideoscaleorc.c gstmvideoscaleorc.h