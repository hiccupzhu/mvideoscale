/*
 * scale.c
 *
 *  Created on: Nov 27, 2012
 *      Author: szhu
 */

#include "scale.h"
#include <assert.h>
#include <liboil/liboil.h>
//#include <orc/orc.h>

//#include "gstmvideoscaleorc.h"
/*
 * 1   2
 * 3   4
 *
 * fx = src_w / des_w
 * fy = src_h / des_h
 */

static const int FACTOR_BITS = 16;
static const int FACTOR = (1 << 16);

//void zoom_map(uint8_t *src, uint8_t *pDstImg, int src_w, int src_h, float rate)
void scale_bilinear(uint8_t *src, int src_w, int src_h, uint8_t *des, int des_w, int des_h)
{
    int dh = 0;
    int dw = 0;

    uint8_t *p1, *p2;
    uint8_t* dY = des;

    int tx, ty;
    int u, v;

    int t1, t2, t3, t4;
    int sx, sy;

    int deltx, delty;

//    float delt = 1 / rate;
    deltx = (src_w << FACTOR_BITS) / des_w;
    delty = (src_h << FACTOR_BITS) / des_h;

    ty = 0;
    for( dh = 0; dh < des_h; dh++, ty += delty)
    {
//        ty = dh / rate;

        v = ty & (FACTOR-1);

        sy = ty >> FACTOR_BITS;
        assert(sy < src_h);

        tx = 0;
        for( dw = 0; dw < des_w; dw++, tx += deltx)
        {
//          tx = dw / rate;
            u = tx & (FACTOR-1);

            sx = tx >> FACTOR_BITS;

            if(sy == (src_h - 1)){
                p1 = src + sy * src_w + sx;
                if(sx < src_w){
                    t1 = (*p1)*u;
                    t2 = *(p1 + 1)*(FACTOR-u);
                    t3 = 0;
                    t4 = 0;
                    *dY ++ = (t1 + t2) >> FACTOR_BITS;
                }else{
                    *dY = *p1;
                }
                continue;
            }

            assert(sx < src_w);

            if(sx == (src_w - 1)){
                p1 = src + sy * src_w + sx;
                p2 = p1 + src_w;

                t1 = *p1;
                t2 = 0;
                t3 = *p2;
                t4 = 0;
                *dY ++ = ((t1)*v >> FACTOR_BITS) + ((t3)*(FACTOR-v) >> FACTOR_BITS);
                continue;
            }

            p1 = src + sy * src_w + sx;
            p2 = p1 + src_w;
            t1 = (*p1)*(u) >> FACTOR_BITS;
            t2 = (*(p1+1))*(FACTOR-u) >> FACTOR_BITS;
            t3 = (*p2)*(u) >> FACTOR_BITS;
            t4 = (*(p2+1))*(FACTOR-u) >> FACTOR_BITS;
            *dY ++ = ((t1 + t2)*v >> FACTOR_BITS) + ((t3 + t4)*(FACTOR-v) >> FACTOR_BITS);

#if 0
            if(sx >= src_w - 1){
                sx = src_w - 2;
                p1 = src + s_off1 + sx;
                p2 = src + s_off2 + sx;

                t1 = (*p1)*(u) >> FACTOR_BITS;
                t2 = (*(p1+1))*(FACTOR-u) >> FACTOR_BITS;
                t3 = (*p2)*(u) >> FACTOR_BITS;
                t4 = (*(p2+1))*(FACTOR-u) >> FACTOR_BITS;
            }else{
                p1 = src + s_off1 + sx;
                p2 = src + s_off2 + sx;
                t1 = (*p1)*(u) >> FACTOR_BITS;
                t2 = (*(p1+1))*(FACTOR-u) >> FACTOR_BITS;
                t3 = (*p2)*(u) >> FACTOR_BITS;
                t4 = (*(p2+1))*(FACTOR-u) >> FACTOR_BITS;
            }
            *dY ++ = ((t1 + t2)*v >> FACTOR_BITS) + ((t3 + t4)*(FACTOR-v) >> FACTOR_BITS);
#endif
        }

    }
}

void scale_linear_image(uint8_t *src, int src_w, int src_h, uint8_t *des, int des_w, int des_h)
{
    int dh = 0;
    int dw = 0;

    int tx, ty;
    int u, v;

    int sx, sy;


//    float delt = 1 / rate;
    int deltx, delty;
    deltx = (src_w << FACTOR_BITS) / des_w;
    delty = (src_h << FACTOR_BITS) / des_h;

    ty = 0;
    for( dh = 0; dh < des_h; dh++, ty += delty)
    {
//        ty = dh / rate;
        v = ty & (FACTOR-1);

        sy = ty >> FACTOR_BITS;
        assert(sy < src_h);

        scale_linear(des + dh * des_w, src + sy * src_w, src_w, des_w, deltx);
    }
}

void scale_bilinear2(uint8_t *pool_buf, uint8_t *src, int src_w, int src_h, uint8_t *des, int des_w, int des_h, int des_left)
{
    int dh;
    int ty, v, sy;
    int deltx, delty;
    int des_off, y1, y2;
    uint8_t *buf1, *buf2;

    if(sizeof(pool_buf) == 8){
        buf1 = (((uint64_t)pool_buf + 0xF) & (~0xF));
    }else{
        buf1 = (((uint32_t)pool_buf + 0xF) & (~0xF));
    }
    buf2 = buf1 + 0x10000;

    des_off = 0;
//    float delt = 1 / rate;
//    deltx = FACTOR * fx;
//    delty = FACTOR * fy;

    deltx = (src_w << FACTOR_BITS) / des_w;
    delty = (src_h << FACTOR_BITS) / des_h;

    y1 = -1;
    y2 = -1;

    scale_linear(buf1, src, src_w, des_w, deltx);

    ty = 0;
    y1 = 0;
    for( dh = 0; dh < des_h; dh++, ty += delty)
    {
//        ty = dh / rate;
        v = ty & (FACTOR-1);
        sy = ty >> FACTOR_BITS;
        assert(sy < src_h);

        if(v == 0){
            if(dh == y1){
                memcpy(des + des_off, buf1, des_w);
            }else if(dh == y2){
                memcpy(des + des_off, buf2, des_w);
            }else{
                scale_linear(buf1, src + sy * src_w, src_w, des_w, deltx);
                memcpy(des + des_off, buf1, des_w);
                y1 = sy;
            }
        }else{
            if(sy == src_h - 1){
                memcpy(des + des_off, buf2, des_w);
                continue;
            }
            if(sy == y1){
                if(sy + 1 != y2){
                    scale_linear(buf2, src + (sy + 1) * src_w, src_w, des_w, deltx);
                    y2 = sy + 1;
                }
                if((v >> 8) == 0){
                    memcpy(des + des_off, buf1, des_w);
                }else{
                    merge_linear_buffer(des + des_off, buf1, buf2, des_w, v);
                }
            }else if(sy == y2){
                if(sy + 1 != y1){
                    scale_linear(buf1, src + (sy + 1) * src_w, src_w, des_w, deltx);
                    y1 = sy + 1;
                }

                if((v >> 8) == 0){
                    memcpy(des + des_off, buf2, des_w);
                }else{
                    merge_linear_buffer(des + des_off, buf1, buf2, des_w, v);
                }
            }else{
                scale_linear(buf1, src + sy * src_w, src_w, des_w, deltx);
                y1 = sy;
                scale_linear(buf2, src + (sy + 1) * src_w, src_w, des_w, deltx);
                y2 = sy + 1;
                if((v >> 8) == 0){
                    memcpy(des + des_off, buf1, des_w);
                }else{
                    merge_linear_buffer(des + des_off, buf1, buf2, des_w, v);
                }
            }

        }

        des_off += des_w + (des_left * 2);
    }
//    free(pool_buf);
}

void scale_linear(uint8_t *dest, uint8_t *src, int src_width, int des_width, int increment){
#if 0
    gst_mvideoscale_orc_resample_bilinear_u8(dest, src, 0, increment, des_width);
#else
    int acc = 0;
    int i;
    int j;
    int x;

    for (i = 0; i < des_width; i++) {
        j = acc >> FACTOR_BITS;
        x = acc & (FACTOR - 1);

        if (j + 1 < src_width)
            dest[i] = (src[j] * (FACTOR - x) + src[j + 1] * x) >> FACTOR_BITS;
        else
            dest[i] = src[j];

        acc += increment;
    }
#endif
}


void merge_linear_buffer(uint8_t *des, uint8_t *s1, uint8_t *s2, int size, int factor){
#if 0
    int i;
    for(i = 0; i < size; i++){
        *des++ = ((*s1 ++)*factor + (*s2 ++)*(FACTOR - factor)) >> FACTOR_BITS;
    }
#elif 1
    factor >>= 8;
    oil_merge_linear_u8 (des, s1, s2, &factor, size);
#else
    orc_merge_linear_u8(des, s1, s2, factor >> 8, size);
#endif
}
