/*
 * vs_image.c
 *
 *  Created on: Dec 18, 2012
 *      Author: szhu
 */

#include "vs_image.h"

int vs_image_init(GstVideoFormat format, uint8_t *buf, int w, int h, VSImage* img){
    int i;
    if(format == GST_VIDEO_FORMAT_I420 ||
       format == GST_VIDEO_FORMAT_YV12 ||
       format == GST_VIDEO_FORMAT_Y444 ||
       format == GST_VIDEO_FORMAT_Y42B ||
       format == GST_VIDEO_FORMAT_Y41B)
    {
        memset(img, 0, sizeof(VSImage));
        img->format = format;
        img->nb_plane = 3;

        w = GST_ROUND_UP_4(w);
        h = GST_ROUND_UP_2(h);

        img->aspect_ratio_u8 = (w << 8) / h;

        img->width = w;
        img->height = h;

        for(i = 0; i < 3; i++){
            if(i == 1){
                w = GST_ROUND_UP_4((w) >> 1);
                h = (h) >> 1;
            }
            img->plane[i].width = (w);
            img->plane[i].height = (h);
        }

        for(i = 0; i < img->nb_plane; i++){
            img->plane[i].size = img->plane[i].width * img->plane[i].height;
        }
        vs_image_set_buffer(img, buf, FALSE);
    }else{
        return -1;
    }
    return 0;
}

int vs_image_set_real(VSImage* des, VSImage* src){
    int i, rel_w, rel_h;
    int top, left;

    rel_w = MIN(des->width, des->height * src->aspect_ratio_u8 >> 8);
    rel_h = MIN(des->height, (des->width << 8) / src->aspect_ratio_u8);

    rel_w = GST_ROUND_UP_4(rel_w);
    rel_h = GST_ROUND_UP_2(rel_h);
    //(A/4 - B/4) = (A-B)/4 !!!
    left = ((des->width - rel_w) >> 1);
    top  = GST_ROUND_UP_2(((des->height - rel_h)) >> 1);
    //for index
//    top  = MAX(0, top - 1);
//    left = MAX(0, left - 1);


    for(i = 0; i < 3; i++){
        if(i == 1){
            rel_w = GST_ROUND_UP_4((rel_w) >> 1);
            rel_h = (rel_h) >> 1;

            left = ((des->plane[i].width - rel_w) >> 1);
            top = (top) >> 1;
        }
        des->plane[i].rel_top = top;
        des->plane[i].rel_left = left;
        des->plane[i].rel_width = rel_w;
        des->plane[i].rel_height = rel_h;
    }
    return 0;
}

int vs_image_set_buffer(VSImage* img, guint8* buf, gboolean buf_is_dest){
    int i;
    if( img->format == GST_VIDEO_FORMAT_I420 ||
        img->format == GST_VIDEO_FORMAT_YV12 ||
        img->format == GST_VIDEO_FORMAT_Y444 ||
        img->format == GST_VIDEO_FORMAT_Y42B ||
        img->format == GST_VIDEO_FORMAT_Y41B)
    {
        img->plane[0].data = buf;
        img->plane[1].data = img->plane[0].data + img->plane[0].size;
        img->plane[2].data = img->plane[1].data + img->plane[1].size;

        for(i = 0; buf_is_dest && i < img->nb_plane; i++){
            img->plane[i].rel_data = img->plane[i].data + img->plane[i].rel_top * img->plane[i].rel_width + img->plane[i].rel_left;
        }
    }else{
        return -1;
    }
    return 0;
}

int vs_image_fill_black(VSImage* img){
    int i;
    const guint8* black = vs_image_get_black(img->format);
    for(i = 0; i < img->nb_plane; i++){
        memset(img->plane[i].data, *(black + i), img->plane[i].size);
    }
    return 0;
}

int vs_image_scale(guint8* pool_buf, VSImage* des, VSImage* src, gboolean adjust_aspect){
    int i;
    if(adjust_aspect){
        vs_image_fill_black(des);
        for(i = 0; i < src->nb_plane; i++)
//        for(i = 0; i < 2; i++)
        {
            scale_bilinear2(pool_buf,
                    src->plane[i].data, src->plane[i].width, src->plane[i].height,
                    des->plane[i].rel_data, des->plane[i].rel_width, des->plane[i].rel_height, des->plane[i].rel_left);
        }
    }else{
//        for(i = 0; i < src->nb_plane; i++)
        for(i = 0; i < src->nb_plane; i++)
        {
            scale_bilinear2(pool_buf,
                    src->plane[i].data, src->plane[i].width, src->plane[i].height,
                    des->plane[i].data, des->plane[i].width, des->plane[i].height, 0);
        }
    }

    return 0;
}

int vs_image_need_scale(VSImage* des, VSImage* src){
    static const int NEED_SCALE = 1;
    static const int NO_SCALE = 0;
    if(des->format == src->format &&
       (des->width != src->width || des->height != src->height))
    {
        return NEED_SCALE;
    }else{
        return NO_SCALE;
    }
}

int vs_image_copy(VSImage* des, VSImage* src){
    int i;
    des->aspect_ratio_u8 = src->aspect_ratio_u8;
    des->format = src->format;
    des->height = src->height;
    des->nb_plane = src->nb_plane;
    des->width = src->width;

    for(i = 0; i < src->nb_plane; i++){
        des->plane[i].height = src->plane[i].height;
        des->plane[i].width = src->plane[i].width;
        des->plane[i].rel_left = src->plane[i].rel_left;
        des->plane[i].rel_top = src->plane[i].rel_top;
        des->plane[i].rel_height = src->plane[i].rel_height;
        des->plane[i].rel_width = src->plane[i].rel_width;
        //this place not need set the "rel_data"
//        des->plane[i].rel_data = src->plane[i].rel_data;
        memcpy(des->plane[i].data, src->plane[i].data, src->plane[i].size);
    }
    return 0;
}

const guint8 *
vs_image_get_black (GstVideoFormat format)
{
  static const guint8 black[][4] = {
    {255, 0, 0, 0},             /*  0 = ARGB, ABGR, xRGB, xBGR */
    {0, 0, 0, 255},             /*  1 = RGBA, BGRA, RGBx, BGRx */
    {255, 16, 128, 128},        /*  2 = AYUV */
    {0, 0, 0, 0},               /*  3 = RGB and BGR */
    {16, 128, 128, 0},          /*  4 = v301 */
    {16, 128, 16, 128},         /*  5 = YUY2, YUYV */
    {128, 16, 128, 16},         /*  6 = UYVY */
    {16, 0, 0, 0},              /*  7 = Y */
    {0, 0, 0, 0}                /*  8 = RGB565, RGB666 */
  };

  switch (format) {
    case GST_VIDEO_FORMAT_ARGB:
    case GST_VIDEO_FORMAT_ABGR:
    case GST_VIDEO_FORMAT_xRGB:
    case GST_VIDEO_FORMAT_xBGR:
//    case GST_VIDEO_FORMAT_ARGB64:
      return black[0];
    case GST_VIDEO_FORMAT_RGBA:
    case GST_VIDEO_FORMAT_BGRA:
    case GST_VIDEO_FORMAT_RGBx:
    case GST_VIDEO_FORMAT_BGRx:
      return black[1];
    case GST_VIDEO_FORMAT_AYUV:
//    case GST_VIDEO_FORMAT_AYUV64:
      return black[2];
    case GST_VIDEO_FORMAT_RGB:
    case GST_VIDEO_FORMAT_BGR:
      return black[3];
//    case GST_VIDEO_FORMAT_v308:
//      return black[4];
    case GST_VIDEO_FORMAT_YUY2:
    case GST_VIDEO_FORMAT_YVYU:
      return black[5];
    case GST_VIDEO_FORMAT_UYVY:
      return black[6];
//    case GST_VIDEO_FORMAT_Y800:
    case GST_VIDEO_FORMAT_GRAY8:
      return black[7];
    case GST_VIDEO_FORMAT_GRAY16_LE:
    case GST_VIDEO_FORMAT_GRAY16_BE:
//    case GST_VIDEO_FORMAT_Y16:
      return NULL;              /* Handled by the caller */
    case GST_VIDEO_FORMAT_I420:
    case GST_VIDEO_FORMAT_YV12:
    case GST_VIDEO_FORMAT_Y444:
    case GST_VIDEO_FORMAT_Y42B:
    case GST_VIDEO_FORMAT_Y41B:
      return black[4];          /* Y, U, V, 0 */
//    case GST_VIDEO_FORMAT_RGB16:
//    case GST_VIDEO_FORMAT_RGB15:
//      return black[8];
    default:
      return NULL;
  }
}

