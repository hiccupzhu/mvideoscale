/**
 * SECTION:element-mvideoscale
 *
 * FIXME:Describe mvideoscale here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! mvideoscale ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include <gst/controller/gstcontroller.h>

#include "gstmvideoscale.h"
#include "scale.h"

GST_DEBUG_CATEGORY_STATIC (MVIDEOSCALE);
#define GST_CAT_DEFAULT MVIDEOSCALE

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_ADJUST_ASPECT,
  PROP_SILENT,
};

static const int FACTOR = 0x10000;
static const int FACTOR_BITS = 16;

#if 0
static GstStaticPadTemplate sink_template =
GST_STATIC_PAD_TEMPLATE (
  "sink",
  GST_PAD_SINK,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (
        "video/x-raw-yuv, "\
            "format = (fourcc) I420, "\
            "width = (int) [ 1, max ], "\
            "height = (int) [ 1, max ], "\
            "framerate = (fraction) [ 0, max ];"
        "video/x-raw-yuv, "\
            "format = (fourcc) YV12, "\
            "width = (int) [ 1, max ], "\
            "height = (int) [ 1, max ], "\
            "framerate = (fraction) [ 0, max ];"
        )
);

static GstStaticPadTemplate src_template =
GST_STATIC_PAD_TEMPLATE (
  "src",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (
        "video/x-raw-yuv, "\
              "format = (fourcc) I420, "\
              "width = (int) [ 1, max ], "\
              "height = (int) [ 1, max ], "\
              "framerate = (fraction) [ 0, max ];"
        "video/x-raw-yuv, "\
              "format = (fourcc) YV12, "\
              "width = (int) [ 1, max ], "\
              "height = (int) [ 1, max ], "\
              "framerate = (fraction) [ 0, max ];"
        )
);
#endif

#undef GST_VIDEO_SIZE_RANGE
#define GST_VIDEO_SIZE_RANGE "(int) [ 1, 32767]"
static GstStaticCaps gst_mvideo_scale_format_caps[] = {
  GST_STATIC_CAPS (GST_VIDEO_CAPS_YUV ("I420")),
  GST_STATIC_CAPS (GST_VIDEO_CAPS_YUV ("YV12"))
};

static void gst_mvideo_scale_base_init (gpointer g_class);
static void gst_mvideo_scale_class_init (GstMVideoScaleClass * klass);
static void gst_mvideo_scale_init (GstMVideoScale *filter);
static void gst_mvideo_scale_finalize (GObject * object);

static void gst_mvideo_scale_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_mvideo_scale_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static GstCaps * gst_mvideo_scale_transform_caps (GstBaseTransform * trans, GstPadDirection direction, GstCaps * caps);
static GstFlowReturn gst_mvideo_scale_transform_ip (GstBaseTransform * base, GstBuffer * outbuf);
static gboolean gst_mvideo_scale_set_caps (GstBaseTransform * trans, GstCaps * in, GstCaps * out);
static void gst_mvideo_scale_fixate_caps (GstBaseTransform * base, GstPadDirection direction, GstCaps * caps, GstCaps * othercaps);
static gboolean gst_mvideo_scale_get_unit_size (GstBaseTransform * trans, GstCaps * caps, guint * size);
static GstFlowReturn gst_mvideo_scale_transform (GstBaseTransform * trans, GstBuffer * in, GstBuffer * out);

#if 0
#define gst_mvideo_scale_parent_class parent_class
G_DEFINE_TYPE (GstMVideoScale, gst_mvideo_scale, GST_TYPE_BASE_TRANSFORM);
#else
static GstElementClass *parent_class = NULL;

GType
gst_mvideo_scale_get_type (void)
{
  static GType mvideo_scale_type = 0;

  if (!mvideo_scale_type) {
    static const GTypeInfo mvideo_scale_info = {
      sizeof (GstMVideoScaleClass),
      gst_mvideo_scale_base_init,
      NULL,
      (GClassInitFunc) gst_mvideo_scale_class_init,
      NULL,
      NULL,
      sizeof (GstMVideoScale),
      0,
      (GInstanceInitFunc) gst_mvideo_scale_init,
    };

    mvideo_scale_type = g_type_register_static (GST_TYPE_BASE_TRANSFORM, "GstMVideoScale", &mvideo_scale_info, 0);
  }
  return mvideo_scale_type;
}
#endif

static void
gst_mvideo_scale_base_init (gpointer g_class){
    int i;
    GstCaps *caps;
    GstPadTemplate *sink_templ, *src_templ;
    GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

    gst_element_class_set_details_simple (element_class,
        "MVideoScale",
        "Generic/Filter",
        "Video scale can change the aspect-ratio",
        "zhushiqi <<zhusq@itv.cn>>");

    caps = gst_caps_new_empty();
    for(i = 0; i < G_N_ELEMENTS(gst_mvideo_scale_format_caps); i++){
        gst_caps_append (caps,
                gst_caps_make_writable(gst_static_caps_get (&gst_mvideo_scale_format_caps[i])));
    }
    sink_templ = gst_pad_template_new("sink", GST_PAD_SINK, GST_PAD_ALWAYS, gst_caps_ref(caps));
    src_templ  = gst_pad_template_new("src",  GST_PAD_SRC, GST_PAD_ALWAYS, gst_caps_ref(caps));

    gst_element_class_add_pad_template(element_class, sink_templ);
    gst_element_class_add_pad_template(element_class, src_templ);
}

/* initialize the mvideoscale's class */
static void
gst_mvideo_scale_class_init (GstMVideoScaleClass * klass)
{
  GST_DEBUG_CATEGORY_INIT (MVIDEOSCALE, "MVIDEOSCALE", 0, "media videoscale log");

  GObjectClass *gobject_class = (GObjectClass *) klass;
  GstBaseTransformClass *base_class = GST_BASE_TRANSFORM_CLASS(klass);

  gobject_class->set_property = gst_mvideo_scale_set_property;
  gobject_class->get_property = gst_mvideo_scale_get_property;
  gobject_class->finalize = gst_mvideo_scale_finalize;

  g_object_class_install_property (gobject_class, PROP_SILENT,
    g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
          FALSE, G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE));

  g_object_class_install_property (gobject_class, PROP_ADJUST_ASPECT,
      g_param_spec_boolean ("keep-source-aspect-ratio", "aspect ratio", "Auto set the video real width/height which reference the source aspect-ratio.",
            TRUE, G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE));

#if 0
  gst_element_class_set_details_simple (base_class,
    "MVideoScale",
    "Generic/Filter",
    "Video scale can change the aspect-ratio",
    "szhu <<zhusq@itv.cn>>");

  gst_element_class_add_pad_template (base_class,
      gst_static_pad_template_get (&src_template));
  gst_element_class_add_pad_template (base_class,
      gst_static_pad_template_get (&sink_template));
#endif

  base_class->transform_caps = GST_DEBUG_FUNCPTR (gst_mvideo_scale_transform_caps);
//  base_class->transform_ip = GST_DEBUG_FUNCPTR (gst_mvideo_scale_transform_ip);
  base_class->transform      = GST_DEBUG_FUNCPTR (gst_mvideo_scale_transform);
  base_class->set_caps     = GST_DEBUG_FUNCPTR (gst_mvideo_scale_set_caps);
//  base_class->fixate_caps = GST_DEBUG_FUNCPTR (gst_mvideo_scale_fixate_caps);
  base_class->get_unit_size  = GST_DEBUG_FUNCPTR (gst_mvideo_scale_get_unit_size);

  base_class->passthrough_on_same_caps = TRUE;

  parent_class = g_type_class_peek_parent (klass);
}


/* initialize the new element
 * initialize instance structure
 */
static void
gst_mvideo_scale_init (GstMVideoScale *filter)
{
    gst_base_transform_set_qos_enabled (GST_BASE_TRANSFORM (filter), TRUE);

    filter->silent = FALSE;
    filter->tbuf = NULL;
    filter->tbuf_size = 0;
    filter->keep_source_aspect = TRUE;
}

static void
gst_mvideo_scale_finalize (GObject * object){
    GstMVideoScale *vs = GST_MVIDEOSCALE(object);
    if(vs->tbuf){
        free(vs->tbuf);
        vs->tbuf = NULL;
    }

    G_OBJECT_CLASS (parent_class)->finalize (G_OBJECT (vs));
}

static GstCaps *
gst_mvideo_scale_transform_caps (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps)
{
    GstCaps *ret;
    GstStructure *structure;

    g_return_val_if_fail (GST_CAPS_IS_SIMPLE (caps), NULL);

    ret = gst_caps_copy (caps);
    structure = gst_structure_copy (gst_caps_get_structure (ret, 0));

    gst_structure_set (structure,
            "width", GST_TYPE_INT_RANGE, 1, G_MAXINT,
            "height", GST_TYPE_INT_RANGE, 1, G_MAXINT, NULL);

    gst_caps_merge_structure (ret, gst_structure_copy (structure));

    /* if pixel aspect ratio, make a range of it */
    if (gst_structure_get_value (structure, "pixel-aspect-ratio")) {
        gst_structure_set (structure,
                "pixel-aspect-ratio", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

        gst_caps_merge_structure (ret, structure);
    } else {
        gst_structure_free (structure);
    }
//    g_print("%s\n", gst_caps_to_string(ret));

//    gst_caps_append_structure (ret, structure);

    return ret;
}

static gboolean
gst_mvideo_scale_parse_caps (GstCaps * caps, GstVideoFormat * format, gint * width, gint * height,
    gboolean * interlaced)
{
  gboolean ret;
  GstStructure *structure;
  guint32 fourcc;

  structure = gst_caps_get_structure (caps, 0);

  ret = gst_structure_get_int (structure, "width", width);
  ret &= gst_structure_get_int (structure, "height", height);

  do{
      if (format){
          ret = gst_structure_get_fourcc(structure, "format", &fourcc);
          if(!ret) { GST_ERROR("%s get fourcc failed\n", __func__); break; }
          *format = gst_video_format_from_fourcc(fourcc);
      }
  }while(0);

  if (interlaced) {
    *interlaced = FALSE;
    gst_structure_get_boolean (structure, "interlaced", interlaced);
  }

  return ret;
}

static gboolean
gst_mvideo_scale_set_caps (GstBaseTransform * trans, GstCaps * in, GstCaps * out)
{
    GstMVideoScale *vs = GST_MVIDEOSCALE(trans);
    gboolean ret;
    int dw, dh, sw, sh;
    int src_size, des_size;

    ret = gst_mvideo_scale_parse_caps(in, &vs->format, &sw, &sh, &vs->interlaced);
    ret &= gst_mvideo_scale_parse_caps (out, NULL, &dw, &dh, NULL);
    if (!ret)
        goto done;

//    dw = GST_ROUND_UP_2(dw);
//    dh = GST_ROUND_UP_2(dh);

    src_size = gst_video_format_get_size (vs->format, sw, sh);
    des_size = gst_video_format_get_size (vs->format, dw, dh);

//    g_print("======>>dw=%d dh=%d des_size=%d\n", dw, dh, des_size);

    if(NULL == vs->tbuf){
        vs->tbuf_size = MAX(src_size, des_size);
        vs->tbuf = malloc(vs->tbuf_size);
        if(vs->tbuf == NULL){
            ret = FALSE; //means no memory
            vs->tbuf_size = 0;
            goto done;
        }
    }
    vs_image_init(vs->format, NULL, sw, sh, &vs->src_img);
    vs_image_init(vs->format, NULL, dw, dh, &vs->des_img);
    if(vs->keep_source_aspect){
        if(vs->des_img.aspect_ratio_u8 != vs->src_img.aspect_ratio_u8){
            vs->keep_source_aspect = TRUE;
            vs_image_set_real(&vs->des_img, &vs->src_img);
        }else{
            vs->keep_source_aspect = FALSE;
        }
    }
//    GST_INFO("+++++++++++interlaced=%d\n", vs->interlaced);
done:
    return ret;
}

static void
gst_mvideo_scale_fixate_caps (GstBaseTransform * base, GstPadDirection direction, GstCaps * caps, GstCaps * othercaps)
{

}

static gboolean
gst_mvideo_scale_get_unit_size (GstBaseTransform * trans, GstCaps * caps,
    guint * size)
{
  GstVideoFormat format;
  gint w, h;

  if (!gst_video_format_parse_caps (caps, &format, &w, &h))
    return FALSE;

//  w = GST_ROUND_UP_4(w);
//  h = GST_ROUND_UP_2(h);
  //in this function the w/h will be aligned,then return the size
  *size = gst_video_format_get_size (format, w, h);

  return TRUE;
}


static void
gst_mvideo_scale_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
    GstMVideoScale *filter = GST_MVIDEOSCALE (object);

    switch (prop_id) {
    case PROP_SILENT:
        filter->silent = g_value_get_boolean (value);
        break;
    case PROP_ADJUST_ASPECT:
        filter->keep_source_aspect = g_value_get_boolean (value);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
gst_mvideo_scale_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
    GstMVideoScale *filter = GST_MVIDEOSCALE (object);

    switch (prop_id) {
    case PROP_SILENT:
        g_value_set_boolean (value, filter->silent);
        break;
    case PROP_ADJUST_ASPECT:
        g_value_set_boolean(value, filter->keep_source_aspect);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static GstFlowReturn
gst_mvideo_scale_transform (GstBaseTransform * trans, GstBuffer * in, GstBuffer * out)
{
    GstMVideoScale *vs = GST_MVIDEOSCALE(trans);

//    g_print("===>>%s[%d]\n", __func__, __LINE__);
    out->mini_object.flags = in->mini_object.flags;

    vs_image_set_buffer(&vs->src_img, GST_BUFFER_DATA(in), FALSE);
    vs_image_set_buffer(&vs->des_img, GST_BUFFER_DATA(out), TRUE);

    switch(vs->format){
    case GST_VIDEO_FORMAT_I420:
    case GST_VIDEO_FORMAT_YV12:
    case GST_VIDEO_FORMAT_Y444:
    case GST_VIDEO_FORMAT_Y42B:
    case GST_VIDEO_FORMAT_Y41B:
        vs_image_scale(vs->tbuf, &vs->des_img, &vs->src_img, vs->keep_source_aspect);
        break;
    default:
        GST_ERROR("This format is not supported.\n");
    }

    return GST_FLOW_OK;
}

//if in_buf same with out_buf, this function will be called by basetransform.
static GstFlowReturn
gst_mvideo_scale_transform_ip (GstBaseTransform * base, GstBuffer * outbuf)
{
  GstMVideoScale *filter = GST_MVIDEOSCALE (base);

  if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_TIMESTAMP (outbuf)))
    gst_object_sync_values (G_OBJECT (filter), GST_BUFFER_TIMESTAMP (outbuf));

  return GST_FLOW_OK;
}




/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
mvideoscale_init (GstPlugin * mvideoscale)
{
  /* initialize gst controller library */
  gst_controller_init(NULL, NULL);

  oil_init();
//  orc_init();

  return gst_element_register (mvideoscale, "mvideoscale", GST_RANK_NONE,
      GST_TYPE_MVIDEOSCALE);
}


#ifndef PACKAGE
#define PACKAGE "mvideoscale"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "mvideoscale",
    "change the video-window size as aspect-ratio", //description
    mvideoscale_init,
    "0.2.0",
    "LGPL",
    "GStreamer",
    "http://gstreamer.net/"
)
