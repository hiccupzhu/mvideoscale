/*
 * zoom_map.h
 *
 *  Created on: Nov 27, 2012
 *      Author: szhu
 */

#ifndef ZOOM_MAP_H_
#define ZOOM_MAP_H_

#include <stdint.h>
#include "vs_image.h"

void scale_bilinear(uint8_t *src, int sw, int sh, uint8_t *des, int dw, int dh);

//void scale_bilinear2(uint8_t *pool_buf, VSImage* des, VSImage* src);
void scale_bilinear2(uint8_t *pool_buf, uint8_t *src, int sw, int sh, uint8_t *des, int dw, int dh, int des_left);

void scale_linear_image(uint8_t *src, int src_w, int src_h, uint8_t *des, int des_w, int des_h);

void scale_linear(uint8_t *des, uint8_t *src, int src_width, int des_width, int increment);
void merge_linear_buffer(uint8_t *des, uint8_t *s1, uint8_t *s2, int size, int factor);

#endif /* ZOOM_MAP_H_ */
